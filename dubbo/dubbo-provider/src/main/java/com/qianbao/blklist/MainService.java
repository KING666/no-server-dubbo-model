package com.qianbao.blklist;


import com.alibaba.dubbo.container.Main;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by cuinn on 2017/6/5.
 */
public class MainService {

    public static void main(String[] args) {
        @SuppressWarnings("resource")
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext(
                new String[] { "conf/dubbo/dubbo-spring-mybatis.xml" });
        ctx.start();

        Logger logger = LogManager.getLogger(MainService.class);

        logger.info("qbaquila-provider dubbo service started.");

        try {
            Main.main(args);
        } catch (Exception e) {
            logger.error("start qbaquila-provider dubbo service error", e);
        }
    }
}
