package com.qianbao.blklist.baidu;

import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

/**
 * Created by cuinn on 2017/6/5.
 */
@Component("testService")
@Service
public class TestServiceImpl implements TestService {
    public TestServiceImpl() {
        System.out.println("开始执行。。。");
    }

    @Override
    public String getValue() {
        return "hello world!";
    }
}
