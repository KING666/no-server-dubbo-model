package com.qianbao.blklist;


import com.qianbao.blklist.baidu.TestService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by cuinn on 2017/6/5.
 */
public class ConsumerService {

    public static void main(String[] args) {
        System.out.println("main,开始执行。。。");
        ApplicationContext ctx = new ClassPathXmlApplicationContext(
                new String[]{"conf/dubbo/dubbo-spring-customer.xml" });
        if (ctx.containsBean("testService")) {
            System.out.println("存在");
            TestService testService = (TestService) ctx.getBean("testService");
            System.out.println(testService.getValue());
        } else {
            System.out.println("结束了");
            return;
        }
    }
}
