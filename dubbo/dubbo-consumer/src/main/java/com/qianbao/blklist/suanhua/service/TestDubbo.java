package com.qianbao.blklist.suanhua.service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.qianbao.blklist.baidu.TestService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by cuinn on 2017/6/5.
 */
public class TestDubbo {
    @Autowired
    private TestService testService1;

    public TestDubbo() {
        System.out.println("customer:开始了");
    }

    public TestDubbo(TestService testService1) {
        this.testService1 = testService1;
    }

    public void test1() {

        System.out.println(testService1.getValue());
    }
}
